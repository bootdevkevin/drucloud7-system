<?php

/**
 * @file
 * Wysiwyg API integration on behalf of Node module.
 */

/**
 * Implementation of hook_INCLUDE_plugin().
 */
function wysiwyg_juicebox_juicebox_plugin() {
  $plugins['juicebox'] = array(
    'vendor url' => 'http://www.tinymce.com/wiki.php/Plugin:juicebox',
    'icon path' => libraries_get_path('juicebox') . '/classic/img',
    'icon file' => 'jb001.png',
    'icon title' => t('Insert Gallery'),
    'js path' => libraries_get_path('juicebox'),
    'settings' => array(),
  );
  return $plugins;
}