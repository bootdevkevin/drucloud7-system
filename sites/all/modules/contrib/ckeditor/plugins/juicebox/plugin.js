/**
 * @file Plugin for inserting Juicebox token.
 */
(function () {
  var pluginRequires = ['fakeobjects'];
  if (Drupal.ckeditor_ver == 3) {
    pluginRequires = ['fakeobjects', 'htmldataprocessor'];
  }

  CKEDITOR.plugins.add(
    'juicebox',
    {
      requires: pluginRequires,
      init: function (editor) {
        editor.addCommand(
          'juicebox',
          {
            exec: function (i) {
              i.insertText("[JUICEBOX_GALLERY]");
            }
          }
        );
      }
    }
  );
})();
