<?php
/**
 * @file
 * drucloud_all_rss.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drucloud_all_rss_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
