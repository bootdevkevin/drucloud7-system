<?php
/**
 * @file
 * drucloud_users.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function drucloud_users_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_page';
  $context->description = '';
  $context->tag = 'druclouduser page';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-cn_user_recent_comments-block' => array(
          'module' => 'views',
          'delta' => 'cn_user_recent_comments-block',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
        'views-cn_user_recent_posts-block_1' => array(
          'module' => 'views',
          'delta' => 'cn_user_recent_posts-block_1',
          'region' => 'content_bottom',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('druclouduser page');
  $export['user_page'] = $context;

  return $export;
}
