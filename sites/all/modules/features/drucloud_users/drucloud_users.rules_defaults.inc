<?php
/**
 * @file
 * drucloud_users.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function drucloud_users_default_rules_configuration() {
  $items = array();
  $items['cu_rules_internal_new_user_notify_mail'] = entity_import('rules_config', '{ "cu_rules_internal_new_user_notify_mail" : {
      "LABEL" : "Internal new user notify mail",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "drucloudUsers", "email" ],
      "REQUIRES" : [ "rules", "mimemail" ],
      "ON" : { "user_insert" : [] },
      "DO" : [
        { "mimemail" : {
            "key" : "cu_rules_internal_new_user_notify_mail",
            "to" : "keithyau@bootdev.com",
            "cc" : "jacky.chang@bootdev.com",
            "subject" : "New user registration at Drucloud_! ",
            "body" : "New user registered as [account:name] with email  [account:mail].",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
