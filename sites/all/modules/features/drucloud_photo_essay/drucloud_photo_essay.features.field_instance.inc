<?php
/**
 * @file
 * drucloud_photo_essay.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function drucloud_photo_essay_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-photo_essay-body'
  $field_instances['node-photo_essay-body'] = array(
    'bundle' => 'photo_essay',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'aside_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'box_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'recent_posts' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 155,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'smart_trim',
        'settings' => array(
          'more_link' => 0,
          'more_text' => 'Read more',
          'summary_handler' => 'full',
          'trim_length' => 150,
          'trim_options' => array(
            'text' => 'text',
          ),
          'trim_suffix' => '...',
          'trim_type' => 'chars',
        ),
        'type' => 'smart_trim_format',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-photo_essay-field_photos'
  $field_instances['node-photo_essay-field_photos'] = array(
    'bundle' => 'photo_essay',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'aside_teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'trending_articles_v2',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'box_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'photo_essay_v2',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'recent_posts' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'article_teaser_v2',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_photos',
    'label' => 'Photos',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'image_field_caption' => 1,
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'multiupload_imagefield_widget',
      'settings' => array(
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_activity_stream_article' => 0,
          'image_activity_stream_comment' => 0,
          'image_article_aside_teaser' => 0,
          'image_article_author' => 0,
          'image_article_header' => 0,
          'image_article_header_mobile' => 0,
          'image_article_related_news' => 0,
          'image_article_teaser' => 0,
          'image_article_up_next' => 0,
          'image_comment_image' => 0,
          'image_comment_user_image' => 0,
          'image_comment_user_picture' => 0,
          'image_community_box_teaser' => 0,
          'image_community_submitted_module' => 0,
          'image_community_teaser' => 0,
          'image_feature_article_image' => 0,
          'image_homepage_featured_article' => 0,
          'image_homepage_hero' => 0,
          'image_homepage_sponsored_article' => 0,
          'image_juicebox_gallery' => 0,
          'image_juicebox_thumb' => 0,
          'image_large' => 0,
          'image_media_image' => 0,
          'image_medium' => 0,
          'image_mobile_profile_page_user_picture' => 0,
          'image_new_comment_user_image' => 0,
          'image_photo_essay' => 0,
          'image_profile_page_user_picture' => 0,
          'image_related_article_header' => 0,
          'image_square_thumbnail' => 0,
          'image_story_stream' => 0,
          'image_story_stream_mobile' => 0,
          'image_story_stream_tablet' => 0,
          'image_teasers_menu' => 0,
          'image_thumbnail' => 0,
          'image_trending_articles' => 0,
          'image_user_picture' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_miw',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'node-photo_essay-field_tags'
  $field_instances['node-photo_essay-field_tags'] = array(
    'bundle' => 'photo_essay',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'aside_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'box_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'inline',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 2,
      ),
      'recent_posts' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tags',
    'label' => 'Tags',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'active_tags',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'mode' => 'single',
        'size' => 60,
      ),
      'type' => 'active_tags_taxonomy_autocomplete',
      'weight' => 34,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Photos');
  t('Tags');

  return $field_instances;
}
