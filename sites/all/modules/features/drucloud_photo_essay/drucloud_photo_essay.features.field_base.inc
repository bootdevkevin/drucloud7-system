<?php
/**
 * @file
 * drucloud_photo_essay.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function drucloud_photo_essay_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_photos'
  $field_bases['field_photos'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_photos',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 's3',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  return $field_bases;
}
